**How to Deploy**
1. Clone this project
2. Open it in Unity v2020.3.15.f1
3. Play the project through the Unity Editor

**Excecutable Drive** <br>
https://drive.google.com/file/d/1xwxj8fPB5TNmeeThGENDc9n269Qr4KtR/view?usp=sharing

**Documentation** <br>
https://gitlab.com/Archer3-5/deco3500-team/-/wikis/home

**Figma Prototype** <br>
https://www.figma.com/file/XkOzsS1wpAKML6aJJwstcX/DECO3500-Prototype?node-id=0%3A1

**Research Log**
- [Log 1: Week06](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Log1)
- [Log 2: Week07](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Log2)
- [Log 3: Week08](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Log3)
- [Log 4: Week09](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Log4)
- [Log 5: Week10](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Log5)
- [Log 6: Week11](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Log6)
- [Sources](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Research-Sources)

**User Testing**
- [User Interview](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/User-Interview)
- [TAM Result](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/TAM-Result)
- [Pen and Paper Testing](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Pen-and-Paper-Testing)

**Prototype Documentation**
- [Figma Prototype](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Figma-Prototype)
- [Functional Prototype](https://gitlab.com/Archer3-5/deco3500-team/-/wikis/Research/Functional-Prototype)

**Promotional Video**
https://drive.google.com/file/d/1dIijGt2NfBsapXyYahNpfVE1UJKC6V5U/view?usp=sharing
