using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

    
    public void LoadSetUserRole()
    {
        SceneManager.LoadScene("SetUSerRole");
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LoadMainMenu2()
    {
        SceneManager.LoadScene("MainMenu2");
    }

    public void ConversationMenu()
    {
        SceneManager.LoadScene("ConversationMenu");
    }

    public void LoadRecording()
    {
        SceneManager.LoadScene("RecordMic");
    }
    public void LoadRecordingFiles()
    {
        SceneManager.LoadScene("RecordFiles");
    }
    public void LoadChatMessaging()
    {
        SceneManager.LoadScene("ChatMessage");
    }
}

