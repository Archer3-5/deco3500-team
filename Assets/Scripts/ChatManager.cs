using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Chat;
using Photon.Pun;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using System;

public class ChatManager : MonoBehaviour, IChatClientListener
{
    // SetUp Variable
    [SerializeField] GameObject ConnectToChat;
    ChatClient chatClient;
    bool isConnected;

    [SerializeField] string userID;
    [SerializeField] GameObject chatPanel;
    string privateReceiver = "";
    string currentChat;
    [SerializeField] InputField chatField;
    [SerializeField] Text chatDisplay;

    #region setup
    public void UsernameOnValueChange(string valueIn)
    {
        userID = valueIn;
    }

    public void ChatConnectOnClick()
    {
        isConnected = true;
        chatClient = new ChatClient(this);

        chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.AppVersion, new AuthenticationValues(userID));
        Debug.Log("Connenting");
    }
    #endregion

    #region ChatFunctions
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isConnected)
        {
            chatClient.Service();
        }

        if (chatField.text != "" && Input.GetKey(KeyCode.Return))
        {
            SubmitPublicChatOnClick();
        }
    }

    private void SubmitPublicChatOnClick()
    {
        chatClient.PublishMessage("RegionChannel", currentChat);
        chatField.text = "";
        currentChat = "";
    }

    public void TypeChatOnValueChange(string valueIn)
    {
        currentChat = valueIn;
    }

    #endregion

    #region callback
    void IChatClientListener.DebugReturn(DebugLevel level, string message)
    {
        throw new System.NotImplementedException();
    }

    void IChatClientListener.OnChatStateChange(ChatState state)
    {
        if (state == ChatState.Uninitialized)
        {
            isConnected = false;
            ConnectToChat.SetActive(true);
            chatPanel.SetActive(false);
        }
    }

    void IChatClientListener.OnConnected()
    {
        Debug.Log("Connected");
        ConnectToChat.SetActive(false);
        chatClient.Subscribe(new string[] { "Channel" });
    }

    void IChatClientListener.OnDisconnected()
    {
        isConnected = false;
        ConnectToChat.SetActive(true);
        chatPanel.SetActive(false);
    }

    void IChatClientListener.OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        string msgs = "";
        for (int i = 0; i < senders.Length; i++)
        {
            msgs = string.Format("{0}: {1}", senders[i], messages[i]);

            chatDisplay.text += "\n" + msgs;

            Debug.Log(msgs);
        }
    }

    void IChatClientListener.OnPrivateMessage(string sender, object message, string channelName)
    {
        throw new System.NotImplementedException();
    }

    void IChatClientListener.OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        throw new System.NotImplementedException();
    }

    void IChatClientListener.OnSubscribed(string[] channels, bool[] results)
    {
        chatPanel.SetActive(true);
    }

    void IChatClientListener.OnUnsubscribed(string[] channels)
    {
        throw new System.NotImplementedException();
    }

    void IChatClientListener.OnUserSubscribed(string channel, string user)
    {
        throw new System.NotImplementedException();
    }

    void IChatClientListener.OnUserUnsubscribed(string channel, string user)
    {
        throw new System.NotImplementedException();
    }
    #endregion

   
}
